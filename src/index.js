import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const myArray = ['apple', 'banana', 'orange'];

const myList = myArray.map((item) => <p>{item}</p>)
ReactDOM.render(myList, document.getElementById('root'));
function Car() {
  return <h2>Hi, I am a Car!</h2>;
}
ReactDOM.render(<Car />, document.getElementById('root'));
function Car1(props) {
  return <h2>I am a {props.color} Car!</h2>;
}
ReactDOM.render(<Car1 color="red" />, document.getElementById('root'));
function Car2() {
  return <h2>I am a Car!</h2>;
}

function Garage() {
  return (
    <>
      <h1>Who lives in my Garage?</h1>
      <Car2 />
    </>
  );
}
ReactDOM.render(<Garage />, document.getElementById('root'));

const myelement = (
  <table>
    <tr>
      <th>Name</th>
    </tr>
    <tr>
      <td>John</td>
    </tr>
    <tr>
      <td>Elsa</td>
    </tr>
  </table>
);

ReactDOM.render(myelement, document.getElementById('root1'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
